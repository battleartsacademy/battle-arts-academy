Battle Arts Academy is a 13,000 sq. ft. facility that was opened in September of 2013 by Mississauga native Anthony Carelli, better known as former WWE Superstar Santino Marella, in conjunction with "Inside Fitness" magazine owner/publisher Terry Friendo.

Address: 4880 Tomken Rd, Mississauga, ON L4W 1J8, Canada

Phone: 289-622-8853

Website: https://www.battleartsacademy.ca
